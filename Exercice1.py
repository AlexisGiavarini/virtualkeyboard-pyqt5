import sys
from PyQt5.Qt import *

class MyMainWindow(QWidget): 

    __n = 0
    __doPaint = False
    
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        # attributs de la fenetre principale
        self.setGeometry(300,300,800,600)
        self.setWindowTitle('My main window')  
        self.rectUtil = self.rect()

        self.initUI()   # appel d'une methode dédiée à la création de l'IHM

    def dessine(self):
        self.__doPaint = True
        self.update()

    def efface(self):
        self.__doPaint = False
        self.update()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        if (self.__doPaint):
            qp.drawLine(0, self.height() / 2, self.width(), self.height() / 2) #Trace un ligne horizontale au milieu de la fenetre
            c = 13
            l = self.width() / c #largeur de chaque colonne

            for x in range(1, c): #Pour chaque colonne
                pX = x * l #point de depart de tracé de la ligne verticale (represente la case)
                qp.drawLine(pX, 0, pX, self.height())  # tracé de la ligne

        qp.end()

    def initUI(self):
        # code pour remplir... n'a pas besoin de rester pour la suite du TP
        w = self.width()
        h = self.height()
        print(str(w)+","+str(h))

        self.__bu1 = QPushButton("Dessiner", self)
        self.__bu2 = QPushButton("Effacer", self)

        self.__bu1.move(15, 15)
        self.__bu2.move(15, 60)

        self.__bu1.clicked.connect(self.dessine)
        self.__bu2.clicked.connect(self.efface)






     

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow() 
    w.show() 
    app.exec_()    # ou de préférence sys.exit(app.exec_()) si vous êtes sous linux
