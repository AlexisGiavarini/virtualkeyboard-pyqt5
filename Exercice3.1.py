import sys
from PyQt5.QtCore import *
from PyQt5 import QtWebEngineWidgets
from PyQt5.Qt import *
from PyQt5.Qt import PYQT_VERSION_STR

class MyBrowser(QWidget):
    quit = pyqtSignal() #Pour fermer les deux fenetres à la fois

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        self.setGeometry(200, 300, 800, 700)
        self.setMinimumSize(QSize(400, 200))
        self.setWindowTitle('PokeBrowser')
        self.view = QtWebEngineWidgets.QWebEngineView(self)
        self.view.resize(self.width(),self.height())

        self.url = 'https://www.pokepedia.fr/Liste_des_Pok%C3%A9mon_par_ordre_alphab%C3%A9tique'
        self.view.load(QUrl(self.url))

    def changeLetter(self, letter):
        newurl = self.url + "#" + letter
        self.view.load(QUrl(newurl))

    def closeEvent(self, event):
        self.quit.emit()

class MyMainWindow(QWidget):

    quit = pyqtSignal() #pour fermer les deux fenetres à la fois
    sendLetter = pyqtSignal(str) #Signal pour transmettre la lettre au browser
    __lettres = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T","U", "V", "W", "X", "Y", "Z"]

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        # attributs de la fenetre principale
        self.setGeometry(1000,300,800,150)
        self.setMinimumSize(QSize(200, 100))
        self.setWindowTitle('My main window')
        self.rectUtil = self.rect()

        self.initUI()   # appel d'une methode dédiée à la création de l'IHM

    def getLetterFromClick(self):
        self.sendLetter.emit(self.sender().text()) #Envoie la lettre qui correspond au bouton cliqué
        print(self.sender().text())


    def initUI(self):
        w = self.width()
        h = self.height()
        print(str(w)+","+str(h))
        print("PyQt version:", PYQT_VERSION_STR)

        grid = QGridLayout()
        k = 0;
        for i in range(0,2):
            for j in range(0,13):
                buLetter = QPushButton(self.__lettres[k])
                buLetter.clicked.connect(self.getLetterFromClick)
                grid.addWidget(buLetter, i, j)
                k +=1

        self.setLayout(grid)

    def closeEvent(self, event):
        self.quit.emit()










if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    b = MyBrowser()
    b.show()
    w.sendLetter.connect(b.changeLetter) #Fais le lien entre le signal emis et la methode
    w.quit.connect(b.closeEvent)
    b.quit.connect(w.closeEvent)
    app.exec_()    # ou de préférence sys.exit(app.exec_()) si vous êtes sous linux
