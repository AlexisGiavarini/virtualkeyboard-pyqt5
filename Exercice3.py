import sys
from PyQt5.QtCore import *
from PyQt5 import QtWebEngineWidgets
from PyQt5.Qt import *
from PyQt5.Qt import PYQT_VERSION_STR

class MyBrowser(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        self.setGeometry(200, 300, 800, 700)
        self.setMinimumSize(QSize(400, 200))
        self.setWindowTitle('PokeBrowser')
        self.view = QtWebEngineWidgets.QWebEngineView(self)
        self.view.resize(self.width(),self.height())

        self.url = 'https://www.pokepedia.fr/Liste_des_Pok%C3%A9mon_par_ordre_alphab%C3%A9tique'
        self.view.load(QUrl(self.url))

    def changeLetter(self, letter):
        newurl = self.url + "#" + letter
        self.view.load(QUrl(newurl))

class MyMainWindow(QWidget):

    __doPaint = True
    __lettres = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    __pos = [] #Pour retrouver la lettre par rapport à la position du clic
    sendLetter = pyqtSignal(str) #Signal pour transmettre la lettre au browser

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        # attributs de la fenetre principale
        self.setGeometry(1000,300,800,600)
        self.setMinimumSize(QSize(200, 100))
        self.setWindowTitle('My main window')
        self.rectUtil = self.rect()

        self.initUI()   # appel d'une methode dédiée à la création de l'IHM


    def dessine(self):
        self.__doPaint = True
        self.update()

    def efface(self):
        self.__doPaint = False
        self.update()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        self.__pos.clear() # On vide le tableau avant de mettre les nouvelles valeurs dues au redimensionnement
        for t in range(0,14): #nombre de colonnes + la position de la ligne "la plus droite" et "la plus a gauche" pour le calcul des coordonnées
            ecart = t * (self.width()/13)
            self.__pos.append(ecart)

        if (self.__doPaint):
            qp.drawLine(0, self.height() / 2, self.width(), self.height() / 2) #Trace une ligne horizontale au milieu de la fenetre
            c = 13
            l = self.width() / c #largeur de chaque colonne
            i = 0
            j = 13

            for x in range(1, c): #Pour chaque colonne
                pX = x * l #point de depart de tracé de la ligne verticale (represente la case)
                qp.drawLine(pX, 0, pX, self.height())  # tracé de la ligne

            for k in range(0, c): #Dans chaque colonne on va placer au centre une lettre de l'alphabet
                pX2 = k * l
                qp.drawText(pX2,0,l,self.height() / 2,Qt.AlignCenter,self.__lettres[i]) #Ligne du haut
                qp.drawText(pX2, self.height() / 2, l, self.height() / 2, Qt.AlignCenter, self.__lettres[j]) #Ligne du bas
                i+=1
                j+=1

        qp.end()

    def mousePressEvent(self, event):
        self.p = event.pos()
        x = event.x()
        y = event.y()

        c = 13
        l = self.width() / c

        for k in range (0,len(self.__pos)-1):
            if(x >  self.__pos[k]  and x <  self.__pos[k+1]):
                if(y < self.height()/2): # Pour les lettres de la moitié superieure
                    print(self.__lettres[k])
                    self.sendLetter.emit(self.__lettres[k])
                else:
                    print(self.__lettres[k+13]) #Pour les lettres de la moitié inferieure
                    self.sendLetter.emit(self.__lettres[k+13])
                break  # Une fois la lettre recuperée (affichée ici) on sort de la boucle

    def keyPressEvent(self,event):
        if event.key() == Qt.Key_D:
            self.dessine()
        if event.key() == Qt.Key_E:
            self.efface()

    def initUI(self):
        w = self.width()
        h = self.height()
        print(str(w)+","+str(h))
        print("PyQt version:", PYQT_VERSION_STR)








if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    b = MyBrowser()
    b.show()
    w.sendLetter.connect(b.changeLetter) #Fais le lien entre le signal emis et la methode
    app.exec_()    # ou de préférence sys.exit(app.exec_()) si vous êtes sous linux
